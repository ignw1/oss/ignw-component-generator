import React from 'react'
import { render } from '@testing-library/react'
import {{pascalCase name}} from '.'

describe('{{pascalCase name}}', () => {
  it('should render the {{pascalCase name}} component', () => {
    const props = {} as any
    const { container } = render(<{{ pascalCase name }} { ...props } />)
    expect(container).toBeTruthy()
  })
})
