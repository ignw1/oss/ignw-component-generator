import React, { FunctionComponent } from 'react'

// @ts-ignore
export interface {{pascalCase name}}Props {}

export const {{pascalCase name}}: FunctionComponent<{{pascalCase name}}Props> = (props) => {
  return (<></>)
}

export default {{pascalCase name}}
